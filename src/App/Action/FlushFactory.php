<?php

namespace App\Action;

use App\Service\ContextGeneratorService;
use Doctrine\Common\Cache\Cache;
use Interop\Container\ContainerInterface;

class FLushFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $pageCache = $container->get(Cache::class);
        /** @var \Triplestore\Service\ObjectManager $om */
        $om = $container->get('Triplestore\ObjectManager');
        $omCache = $om->getCache();
        $generator = $container->get(ContextGeneratorService::class);

        return new FlushAction($pageCache, $omCache, $generator);
    }
}
