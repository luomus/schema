<?php

namespace App\Action;

use App\Service\ContextGeneratorService;
use App\Service\DocumentService;
use Doctrine\Common\Cache\Cache;
use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\Common\Cache\FlushableCache;
use Zend\Cache\Storage\StorageInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Template;

class FlushAction
{
    private $pageCache;
    private $omCache;
    private $contextGenerator;

    public function __construct(Cache $pageCache, StorageInterface $omCache, ContextGeneratorService $contextGenerator)
    {
        $this->pageCache = $pageCache;
        $this->omCache = $omCache;
        $this->contextGenerator = $contextGenerator;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        if ($this->pageCache instanceof FlushableCache) {
            $this->pageCache->flushAll();
        }
        if (method_exists($this->omCache, 'flush')) {
            $this->omCache->flush();
        }
        $context = $this->contextGenerator->generateAllContext();
        return new JsonResponse([
            'acknowledge' => true,
            'context' => $context
        ]);
    }

}
