<?php

namespace App\Action;

use App\Service\DocumentService;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class DocsPageFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $template =  $container->get(TemplateRendererInterface::class);
        $document = $container->get(DocumentService::class);

        return new DocsPageAction($template, $document);
    }
}
