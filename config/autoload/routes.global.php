<?php

return [
    'dependencies' => [
        'invokables' => [
            Zend\Expressive\Router\RouterInterface::class => Zend\Expressive\Router\FastRouteRouter::class,
        ],
        'factories' => [
            App\Action\HomePageAction::class => App\Action\HomePageFactory::class,
            App\Action\DocsPageAction::class => App\Action\DocsPageFactory::class,
            App\Action\FlushAction::class => App\Action\FlushFactory::class,
        ],
    ],

    'routes' => [
        [
            'name' => 'home',
            'path' => '/',
            'middleware' => App\Action\HomePageAction::class,
            'allowed_methods' => ['GET'],
        ],
        [
            'name' => 'docs',
            'path' => '/docs',
            'middleware' => App\Action\DocsPageAction::class,
            'allowed_methods' => ['GET'],
        ],
        [
            'name' => 'flush',
            'path' => '/flush-cache',
            'middleware' => App\Action\FlushAction::class,
            'allowed_methods' => ['GET'],
        ],
    ],
];
