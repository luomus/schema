<?php

namespace App\Service;


use Interop\Container\ContainerInterface;

class ContextGeneratorServiceFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var \Triplestore\Service\ObjectManager $om */
        $om =  $container->get('Triplestore\ObjectManager');
        $config = $container->get('Config');
        $dir = isset($config['context']['dir']) ? $config['context']['dir'] : './public/context';
        return new ContextGeneratorService(
            $om->getMetadataService(),
            $dir
        );
    }
}