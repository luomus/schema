<?php
namespace App\Service;

use Common\Service\IdService;
use Triplestore\Model\Metadata;
use Triplestore\Model\Properties;
use Triplestore\Service\MetadataService;

class ContextGeneratorService
{
    private $metadataService;
    private $targetDir;

    public function __construct(MetadataService $metadataService, $targetDir)
    {
        $this->metadataService = $metadataService;
        if (!is_dir($targetDir) || !is_writable($targetDir)) {
            throw new \Exception("Directory $targetDir is not writable!");
        }
        $this->targetDir = $targetDir;
    }

    public function generateAllContext() {
        $classes = $this->metadataService->getAllClasses();
        foreach ($classes as $class => $metadata) {
            if (!$this->generateJsonLDContext($class, $metadata)) {
                return false;
            }
        }
        if (!$this->generateGenericJsonLDContext()) {
            return false;
        }
        return true;
    }

    public function generateGenericJsonLDContext() {
        $propHelper = $this->metadataService->getAllProperties();
        $propHelper->getProperties();
        $classes = [
            'generic' => null,
            'generic-en' => 'en',
            'generic-fi' => 'fi',
            'generic-sv' => 'sv'
        ];
        foreach($classes as $file => $lang) {
            $data = $this->getPropertyBody($propHelper, $propHelper->getProperties(), $lang);
            $data = array_merge($this->getBase(true), $data);
            $json = json_encode(['@context' => $data], JSON_UNESCAPED_SLASHES);
            $filename = $this->targetDir . DIRECTORY_SEPARATOR . $file . '.jsonld';
            if (file_put_contents($filename, $json) === false) {
                return false;
            }
        }
        return true;
    }

    public function generateJsonLDContext($class, Metadata $metadata) {
        $langs = [
            '' => null,
            '-en' => 'en',
            '-fi' => 'fi',
            '-sv' => 'sv'
        ];
        foreach($langs as $suffix => $lang) {
            $class = $this->getDotless($class);
            $data  = $this->getContextBody($metadata, $lang);
            if (!isset($data['@context'])) {
                continue;
            }
            //$json = Json::encode($data, false, [JSON_UNESCAPED_SLASHES]);
            $json = json_encode($data, JSON_UNESCAPED_SLASHES);
            $filename = realpath($this->targetDir). DIRECTORY_SEPARATOR . $class . $suffix . '.jsonld';
            if (file_put_contents($filename, $json) === false) {
                return false;
            }
        }
        return true;
    }

    private function getBase($extended = false) {
        $defaultDomain = IdService::getUri(IdService::DEFAULT_DB_QNAME_PREFIX);
        $result = [
            'id' => '@id',
            '@vocab' => $defaultDomain,
            '@base' => $defaultDomain,
        ];
        $names = IdService::getAllNS();
        foreach($names as $ns) {
            if ($ns.':' === IdService::DEFAULT_DB_QNAME_PREFIX) {
                continue;
            }
            $result[$ns] = IdService::getUri($ns.':');
        }
        if ($extended) {
            $result['parentId'] = [
                '@id' => 'http://tun.fi/MY.isPartOf',
                '@type' => '@id'
            ];
            $result['informalTaxonGroups'] = [
                '@id' => 'http://tun.fi/MX.isPartOfInformalTaxonGroup',
                '@type' => '@id'
            ];
            $result['checklist'] = [
                '@id' => 'http://tun.fi/MX.nameAccordingTo',
                '@type' => '@id'
            ];
            $result['alternativeVernacularNames'] = [
                '@id' => 'http://tun.fi/MX.alternativeVernacularName',
                '@type' => '@id'
            ];
            $result['typesOfOccurrenceInFinland'] = [
                '@id' => 'http://tun.fi/MX.typeOfOccurrenceInFinland',
                '@type' => '@id'
            ];
            $result['typesOfOccurrenceInFinland'] = [
                '@id' => 'http://tun.fi/MX.typeOfOccurrenceInFinland',
                '@type' => '@id'
            ];
            $result['occurrenceInFinlandPublications'] = [
                '@id' => 'http://tun.fi/MX.occurrenceInFinlandPublication',
                '@type' => '@id'
            ];
            $result['administrativeStatuses'] = [
                '@id' => 'http://tun.fi/MX.hasAdminStatus',
                '@type' => '@id'
            ];
            $result['taxonConceptId'] = [
                '@id' => 'http://tun.fi/MX.circumscription',
                '@type' => '@id'
            ];

        }
        return $result;
    }

    private function getContextBody(Metadata $metadata, $lang = null) {
        IdService::setDefaultQNamePrefix(IdService::DEFAULT_DB_QNAME_PREFIX);
        $properties = $metadata->getProperties();
        $propHelper = $metadata->getAllProperties();
        $children = $metadata->getChildren();
        $propContext = $this->getPropertyBody($propHelper, $properties, $lang);
        if (empty($propContext)) {
            return [];
        }
        $result = $this->getBase();
        $result = array_merge($result, $propContext);
        foreach($children as $child) {
            $newName = rtrim($this->getDotless($child), 's') . 's';
            $result[$newName] = [
                '@id' => rtrim(IdService::getUri($child), 's') . 's',
                '@container' => '@set'
            ];
            $childMeta = $this->metadataService->getMetadataFor($child);
            if (!$childMeta instanceof Metadata) {
                continue;
            }
            $childContect = $this->getContextBody($childMeta);
            if (isset($childContect['@context'])) {
                $result = array_replace_recursive($childContect['@context'], $result);
            }
        }
        foreach ($properties as $property) {
            if ($metadata->getAllProperties()->isEmbeddable($property)) {
                $newName = $this->getDotless($property);
                $result[$newName] = [
                    '@id' => IdService::getUri($property),
                    '@container' => '@set'
                ];
                $childMeta = $this->metadataService->getMetadataFor($metadata->getAllProperties()->getType($property));
                if (!$childMeta instanceof Metadata) {
                    continue;
                }
                $childContect = $this->getContextBody($childMeta);
                if (isset($childContect['@context'])) {
                    $result = array_replace_recursive($childContect['@context'], $result);
                }
            }
        }
        return ['@context' => $result];
    }

    private function getPropertyBody(Properties $propHelper, $properties, $lang = null) {
        $result = [];
        foreach($properties as $property) {
            if ($property === 'MY.isPartOf') {
                continue;
            }
            $uri = IdService::getUri($property);
            $type = $propHelper->getType($property);
            $dotLessProperty = $this->getDotless($property);
            if (empty($type)) {
                continue;
            }
            if ($this->metadataService->altExists($type)) {
                $result[$dotLessProperty] = [
                    '@id' => $uri,
                    '@type' => '@id'
                ];
            } elseif ($propHelper->isMultiLanguage($property) && $lang !== false) {
                if ($lang === null) {
                    $result[$dotLessProperty] = [
                        '@id' => $uri,
                        '@container' => '@language'
                    ];
                } elseif (is_string($lang)) {
                    $result[$dotLessProperty] = [
                        '@id' => $uri,
                        '@language' => $lang
                    ];
                }
            } else {
                if (strpos($type, IdService::QNAME_PREFIX_XSD) === 0) {
                    $result[$dotLessProperty] = [
                        '@id' => $uri,
                        '@type' => $type
                    ];
                } else {
                    $result[$dotLessProperty] = [
                        '@id' => $uri,
                        '@type' => '@id'
                    ];
                }
            }
        }
        return $result;
    }

    private function getVocab($property, $type, &$result) {
        $alts = $this->metadataService->getAlt($type);
        $spot = $this->getSpecialCharSpot($property);
        $vocab = '';
        if ($spot !== false) {
            $vocab = substr($property, 0, $spot);
        }
        if (strpos(key($alts), $property) === 0) {
            $vocab = $property;
        }
        if (!empty($vocab)) {
            $result['@vocab'] = 'http://tun.fi/' . $vocab;
        }
    }

    private function getSpecialCharSpot($value) {
        $pos = strpos($value, '.');
        if ($pos !== false) {
            return $pos;
        }
        $pos = strpos($value, ':');
        if ($pos !== false) {
            return $pos;
        }
        return false;
    }

    private function getDotless($value) {
        $pos = $this->getSpecialCharSpot($value);
        if ($pos !== false) {
            $value = substr($value, $pos + 1);
        }
        return $value;
    }

}