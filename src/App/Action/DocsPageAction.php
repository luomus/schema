<?php

namespace App\Action;

use App\Service\DocumentService;
use Zend\Diactoros\Response\HtmlResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Expressive\Template;

class DocsPageAction
{
    private $template;
    private $documentService;

    public function __construct(Template\TemplateRendererInterface $template, DocumentService $documentService)
    {
        $this->template = $template;
        $this->documentService = $documentService;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $data = [];
        $params = $request->getQueryParams();
        if (isset($params['q'])) {
            return $this->doSearch($params['q']);
        } else if (isset($params['class'])) {
            return $this->addCache($this->doClassPage($response, 'app::class-page', $params['class']));
        } elseif (isset($params['alt'])) {
            $data['alt'] = $params['alt'];
            $data['values'] = $this->documentService->getAltDocumentation($params['alt']);
            $html = $this->template->render('app::alt-page', $data);
            $response->getBody()->write($html);
            return $this->addCache($response);
        }
        return $this->addCache($this->doClassPage($response));
    }

    /**
     * @param $query
     * @return HtmlResponse
     */
    private function doSearch($query) {
        $data = [];
        $data['results'] = $this->documentService->search($query);
        $data['query'] = $query;
        return new HtmlResponse($this->template->render('app::search-result', $data));
    }

    private function doClassPage( ResponseInterface $response, $tpl = 'app::classes-page', $class = null) {
        $data = $this->documentService->getClassDocumentation($class);
        $data['class'] = $class;
        $html = $this->template->render($tpl, $data);
        $response->getBody()->write($html);
        return $response;
    }

    private function addCache(ResponseInterface $response) {
        return $response
            ->withHeader('Content-Type', 'text/html')
            ->withHeader('Cache-Control', ['public', 'max-age=3600']);
    }
}
