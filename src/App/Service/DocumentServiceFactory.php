<?php

namespace App\Service;


use Interop\Container\ContainerInterface;

class DocumentServiceFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $om =  $container->get('Triplestore\ObjectManager');

        return new DocumentService($om);
    }
}