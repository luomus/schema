<?php
namespace App\Service;

use Triplestore\Model\Metadata;
use Triplestore\Model\Model;
use Triplestore\Service\ObjectManager;

class DocumentService
{

    private $om;
    private $lang = 'en';

    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    public function search($query)
    {
        $om = $this->om;
        $metadataService = $om->getMetadataService();
        $classes = $metadataService->getAllClasses();
        $result = ['classes' => [],'properties' => [], 'types' => []];
        $query = strtolower($query);
        foreach($classes as $class => $meta) {
            /** @var Metadata $meta */
            $properties = $meta->getProperties();
            if (count($properties) === 0) {
                continue;
            }
            if (strpos(strtolower($class), $query) !== false) {
                $result['classes'][] = $class;
            }
            foreach($properties as $property) {
                if (strpos(strtolower($property), $query) !== false) {
                    if (!isset($result['properties'][$property])) {
                        $result['properties'][$property] = [];
                    }
                    $result['properties'][$property][] = $class;
                }
                $type = $meta->getType($property);
                if (strpos(strtolower($type), $query) !== false) {
                    if (!isset($result['types'][$type])) {
                        $result['types'][$type] = [];
                    }
                    $result['types'][$type][] = [
                        'class' => $class,
                        'property' => $property
                    ];
                }
            }

        }
        return $result;
    }

    public function getClassDocumentation($class = null)
    {
        $om = $this->om;
        $metadataService = $om->getMetadataService();
        $classes = $metadataService->getAllClasses();
        if ($class !== null) {
            $metadata = $metadataService->getMetadataFor($class);
            if ($metadata === null) {
                return ['classData' => '', 'properties' => ''];
            }
            return [
                'classData' => $this->getClassData($class),
                'properties' => $this->extractPropertyMetadata($metadata, $classes)
            ];

        }

        $result = [];
        foreach($classes as $class => $metadata) {
            /** @var Metadata $metadata */
            if (count($metadata->getProperties())) {
                $result[$class] = $this->getClassData($class);
            }
        }
        uksort($result, [$this, 'cmpAlpha']);

        return ['classes' => $result];
    }

    private function cmpAlpha($a, $b)
    {
        $partsA = preg_split("/[:\.]/", strtolower($a), 2);
        $a = isset($partsA[1]) ? $partsA[1] : $partsA[0];
        $partsB = preg_split("/[:\.]/", strtolower($b), 2);
        $b = isset($partsB[1]) ? $partsB[1] : $partsB[0];

        return strcmp($a, $b);
    }

    private function getClassData($class)
    {
        $om = $this->om;
        $om->disableHydrator();
        $model = $om->fetch($class);
        if ($model === null) {
            return ['label' => '', 'comment' => ''];
        }
        $comment = $this->getFieldValue($model, ObjectManager::PREDICATE_COMMENT);
        $label = $this->getFieldValue($model, ObjectManager::PREDICATE_LABEL);
        return [
            'label' => $label,
            'comment' => $comment
        ];
    }

    private function getFieldValue(Model $model, $field) {
        $value = $model->getOrCreatePredicate($field)->getLiteralValue($this->lang);
        if (isset($value[0])) {
            return $value[0];
        }
        return '';
    }

    public function extractPropertyMetadata(Metadata $metadata, $classes) {
        $data = $metadata->getProperties();
        $result = [];
        foreach($data as $property) {
            $desc = $metadata->getHelp($property);
            $type = $metadata->getType($property);
            $label = $metadata->getLabel($property);
            if (empty($label)) {
                $label = $metadata->getLabel($_POST, 'fi');
            }
            $result[$property] = [
                'label' => $label,
                'type' => $type,
                'type-link' => $this->getTypeLink($type, $classes),
                'description' => $desc,
                'required' => $metadata->isRequired($property),
                'many' => $metadata->hasMany($property)
            ];
        }
        uksort($result, [$this, 'cmpAlpha']);

        return $result;
    }

    public function getAltDocumentation($alt)
    {
        return $this->buildFromOnto($alt);
    }

    private function getTypeLink($type, $classes) {
        if (strpos($type, '.') !== false) {
            if (isset($classes[$type])) {
                return '?class=' . $type;
            }
            return '?alt=' . $type;
        }
        return '';
    }

    private function buildFromOnto($field, $allowEmpty = false)
    {
        $om = $this->om;
        $om->disableHydrator();
        $mainModel = $om->fetch($field, false);
        if ($mainModel === null) {
            throw new \RuntimeException(sprintf("No values found for %s in triplestore.", $field));
        }
        $type = $mainModel->getPredicate(ObjectManager::PREDICATE_TYPE)->current()->getValue();
        if ($type === ObjectManager::OBJ_TYPE_SELECT) {
            $fieldsModels = $mainModel;
        } else if ($type === ObjectManager::OBJ_TYPE_PROPERTY) {
            $rangePred = $mainModel->getPredicate(ObjectManager::PREDICATE_RANGE);
            if ($rangePred === null) {
                throw new \RuntimeException(sprintf("No range found for the field '%s' in triplestore.", $field));
            }
            $range = $rangePred->current()->getValue();
            $fieldsModels = $om->fetch($range, false);
            if ($fieldsModels === null) {
                throw new \RuntimeException(sprintf("Nothing found with range '%s' in triplestore.", $range));
            }
        } else {
            throw new \RuntimeException(sprintf("Unable to determinate the type of '%s'.", $field));
        }

        $ordered = [];
        $group = 0;
        foreach ($fieldsModels as $predicate) {
            /** @var \Triplestore\Model\Predicate $predicate */
            $name = $predicate->getName();
            if (strpos($name, 'rdf:_') !== 0) {
                continue;
            }
            $spot = (int)str_replace('rdf:_', '', $name);
            if ($predicate->count() > 1) {
                throw new \RuntimeException("Found ambiguous information for '$name'");
            }
            $value = $predicate->current()->getValue();
            /** @var Model $optionModel */
            $optionModel = $om->fetch($value, false);
            if ($optionModel !== null) {
                $labelPred = $optionModel->getOrCreatePredicate(ObjectManager::PREDICATE_LABEL);
                if ($labelPred !== null) {
                    $labels = $labelPred->getLiteralValue($this->lang);
                }
                if (empty($labels)) {
                    $labels = $labelPred->getLiteralValue('fi');
                }
            } else {
                $optionModel = new Model();
            }
            if (empty($labels)) {
                $labels = array($value);
            }
            $child = null;
            if ($optionModel->getPredicate('rdf:_1') !== null) {
                $child = $this->buildFromOnto($value, true);
            }
            if ($child == null) {
                $ordered[$spot] = array(
                    'key' => $value,
                    'value' => array_pop($labels)
                );
            } else {
                $group++;
                $ordered[$spot] = array(
                    'key' => 'group' . $group,
                    'value' => array(
                        'label' => array_pop($labels),
                        'options' => $child,
                    )
                );
            }
        }
        if (count($ordered) === 0) {
            if ($allowEmpty) {
                return null;
            }
            throw new \RuntimeException(sprintf("No data found for field %s.", $field));
        }
        ksort($ordered, SORT_NUMERIC);
        $return = array();
        foreach ($ordered as $data) {
            $return[$data['key']] = $data['value'];
        }

        return $return;
    }
}